---
title: "The Great Gatsby Bootcamp"
date: "2020-01-25"
---

I just launched a new bootcamp!

![Grass](./grass.jpg)

## Topic Covered

1. Gatsby
2. GraphQL
3. React
