import React from 'react';
import Header from './Header';
import Footer from './Footer';
import '../styles/index.scss';
import s from './layout.module.scss';

export default function Layout({ children }) {
	return (
		<div className={s.container}>
			<div className={s.content}>
				<Header />
				{children}
			</div>
			<Footer />
		</div>
	);
}