import React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import s from './footer.module.scss';

export default function Footer() {
	const data = useStaticQuery(graphql`
		query {
			site {
				siteMetadata {
					author
				}
			}
		}
	`);

	return (
		<footer className={s.footer}>
			<p>Created by {data.site.siteMetadata.author}, © 2020</p>
		</footer>
	)
}