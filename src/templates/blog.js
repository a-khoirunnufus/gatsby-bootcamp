import React from 'react';
import Layout from '../components/Layout';
import { graphql } from 'gatsby';
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';
import Head from '../components/Head';

export const query = graphql`
	query($slug: String!) {
		contentfulBlogPost(slug: { eq: $slug }) {
			title
			publishedDate(formatString: "MMMM Do, YYYY")
			body {
				raw
				references {
          title
          file {
            url
          }
        }
			}
		}
	}
`

export default function Blog(props) {
	const options = {
		renderNode: {
			"embedded-assets-block": node => {
				const alt = node.data.contentfulBlogPost.body.references['title'];
				const url = node.data.contentfulBlogPost.body.references['file'].url;
				return (
					<img alt={alt} src={url} />
				)
			}
		}
	};

	return (
		<Layout>
			<Head title={props.data.contentfulBlogPost.title} />
			<h1>{props.data.contentfulBlogPost.title}</h1>
			<p>{props.data.contentfulBlogPost.publishedDate}</p>
			{ documentToReactComponents(JSON.parse(props.data.contentfulBlogPost.body.raw), options) }
		</Layout>
	)
}