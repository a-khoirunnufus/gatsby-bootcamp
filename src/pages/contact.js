import React from 'react';
import Layout from '../components/Layout';
import Head from '../components/Head';

export default function AboutPage() {
	return (
		<Layout>
			<Head title="Contact" />
			<h1>Contact</h1>
			<ul>
				<li>twitter: <a href="https://twitter.com" target="_blank" rel="noreferrer">open twitter profile</a></li>
				<li>email: a.khoirunnufus@gmail.com</li>
				<li>phone: +6282182948172</li>
			</ul>
		</Layout>
	);
}