import React from 'react';
import { Link } from 'gatsby';
import Layout from '../components/Layout';
import Head from '../components/Head';

export default function AboutPage() {
	return (
		<Layout>
			<Head title="About" />
			<h1>About Me</h1>
			<ul>
				<li>Name: Ahmad Khoirunnufus</li>
				<li>Age: 20 years old</li>
				<li>Status: Student</li>
				<li>Hobby: Playing games, Coding</li>
			</ul>
			<p>Contact me <Link to="/contact">here</Link>.</p>
		</Layout>
	);
}