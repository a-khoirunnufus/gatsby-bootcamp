import React from "react";
import { Link } from 'gatsby';
import Layout from '../components/Layout';
import Head from '../components/Head';

export default function HomePage() {
  return (
		<Layout>
			<Head title="Home" />
  		<h1>Hello,</h1>
  		<h2>I'm Ahmad, a front-end developer living in indonesia.</h2>
  		<p>Need a developer? <Link to="/contact">Contact me.</Link></p>
		</Layout>
  );
}
