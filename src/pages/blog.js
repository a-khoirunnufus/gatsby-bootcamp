import React from 'react';
import Layout from '../components/Layout';
import { Link, graphql, useStaticQuery } from 'gatsby';
import s from './blog.module.scss';
import Head from '../components/Head';

export default function BlogPage() {
	const data = useStaticQuery(graphql`
		query {
		  allContentfulBlogPost (
		    filter : {
		      node_locale: {
		        eq: "en-US"
		      }
		    }
		    sort : {
		      fields:publishedDate,
		      order:DESC
		    }
		  ) {
		    edges {
		      node {
		        title
		        slug
		        publishedDate(formatString:"MMMM Do, YYYY")
		      }
		    }
		  }
		}
	`);

	return (
		<Layout>
			<Head title="Blog" />
			<h1>Blog</h1>
			<ol className={s.posts}>
			{
				data.allContentfulBlogPost.edges.map((edge, key) => (
					<li key={key} className={s.post}>
						<Link to={`/blog/${edge.node.slug}`}>
							<h2> 
								{edge.node.title}								
							</h2>
							<p>{edge.node.publishedDate}</p>
						</Link>
					</li>
				))
			}
			</ol>
		</Layout>
	);
}